/*
    이미지 병합 스크립트

    //npm install minimist
    //npm install canvas

*/

// 간단한 검사 함수
var isNaN = function(value){ if( value == "" ||value == null || value == undefined || ( value != null && typeof value == "object" && !Object.keys(value).length ) ){ return true }else{ return false } };
var isString = function(value){if(typeof value == 'string') {return true;} else {return false;} }
var isRightArgv = function(value) {if(!isNaN(value) && isString(value)) return true; else return false;}
var isImageFile = function(ext) {if(ext==='.png' || ext==='.jpg' || ext==='.jpeg') return true; else return false;}

// 특정 처리에 사용되는 함수
var loadImageFromDirectory = function(dir){

    var images = new Array();

    try{
        fs.readdirSync(dir).forEach(function(element){
            if(isImageFile(path.extname(element)))
                images.push(path.join(dir, path.dirname(element), element)); //경로 재조합 후 샘플 이미지 배열에 삽입
        });
    }catch (exception){
        console.log(dir +'에서 이미지 목록을 불러오는 중 오류 발생');
        console.log(exception.message);
        process.exit(1);
    }

    console.log(images);
    return images;
}

// 안드로이드 화면 관련 정보 정의
var android_screen_info = {
    'x' : 29,
    'y' : 125,
    'width' : 530,
    'height': 920
};

// 아이폰 화면 관련 정보 정의
var iphone_screen_info = {
    'x' : 73,
    'y' : 218,
    'width' : 752,
    'height': 1333
};


// 파라미터 받아오기
var argv = require('minimist')(process.argv.slice(2), opts="opts.string");

var model_dir = './frame/';
var sample_dir = argv['s'];
var dest_dir = argv['d'];

// 파라미터 검사
if(!isRightArgv(sample_dir) || !isRightArgv(dest_dir)){
    console.log('파라미터가 부족하거나 잘못되었습니다.');
    console.log('frame -s Sample_directory -d Destination_directory');
    process.exit(1);
}

var model_image = new Array();      // 배경 이미지 리스트
var sample_images = new Array();    // 화면 이미지 리스트

// 파일, 경로 관련 모듈 로드
var fs = require('fs');
var path = require('path');


console.log('모델이미지 목록을 가져오는 중 입니다.');
model_image = loadImageFromDirectory(model_dir);
console.log('샘플이미지 목록을 가져오는 중 입니다.');
sample_images = loadImageFromDirectory(sample_dir);


// 이미지 처리 모듈 jimp 로드
var jimp = require('jimp');

// 이미지 병합
sample_images.forEach(function(sample){
    model_image.forEach(function(model){

        var jimps = Array();

        //배경, 화면 캡쳐 순으로 배열에 삽입
        jimps.push(jimp.read(model), jimp.read(sample));

        Promise.all(jimps).then(function(data){
            return Promise.all(jimps);
        }).then(function(data){

            // 파일 이름
            var basename = path.basename(sample);
            var output_name = '';

            if(model.indexOf('android') !== -1){
                data[1].resize(android_screen_info['width'], android_screen_info['height']);        // 이미지 리사이즈
                data[0].composite(data[1], android_screen_info['x'], android_screen_info['y']);     // 배경에 맞게 이미지 병합
                output_name = path.join(dest_dir + basename.replace(path.extname(sample), '_android') + '.png');
            }
            else if(model.indexOf('iphone') !== -1){
                data[1].resize(iphone_screen_info['width'], iphone_screen_info['height']);
                data[0].composite(data[1], iphone_screen_info['x'], iphone_screen_info['y']);
                output_name = path.join(dest_dir + basename.replace(path.extname(sample), '_iphone') + '.png');
            }else{
                data[0].composite(data[1], 0, 0);   //이도저도 아닌경우 그냥 0, 0 위치에 넣어서 누가봐도 오류처럼 보이게 처리
            }

            data[0].write(output_name, function(){
                console.log('write imgae! : '+ basename);
            });
        });

    });
});
